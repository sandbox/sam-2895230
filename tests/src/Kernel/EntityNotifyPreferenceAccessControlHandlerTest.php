<?php

namespace Drupal\Tests\entity_notify\Kernel;

use Drupal\entity_notify\Entity\EntityNotifyPreference;
use Drupal\KernelTests\KernelTestBase;
use Drupal\simpletest\UserCreationTrait;

/**
 * Test the notification entity access control handler.
 *
 * @group entity_notify
 */
class EntityNotifyPreferenceAccessControlHandlerTest extends KernelTestBase {

  use UserCreationTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'system',
    'entity_notify',
    'user',
    'entity_reference',
    'options',
  ];

  /**
   * The access control handler.
   *
   * @var \Drupal\Core\Entity\EntityAccessControlHandlerInterface
   */
  protected $accessHandler;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->installSchema('system', ['sequences']);
    $this->installConfig(['entity_notify']);
    $this->installEntitySchema('user');
    $this->installEntitySchema('entity_notify_preference');
    $this->accessHandler = $this->container->get('entity_type.manager')->getAccessControlHandler('entity_notify_preference');

    // The first user created in kernel tests has UID = 1 and bypasses all
    // access control.
    $this->createUser();
  }

  /**
   * Test the entity access control handler.
   */
  public function testAccess() {
    $entity = EntityNotifyPreference::create([
      'entity_type' => 'foo',
    ]);
    $entity->save();

    // Admins can manage all entities.
    $admin = $this->createUser(['administer entity notifications']);
    $this->assertTrue($entity->access('update', $admin));
    $this->assertTrue($entity->access('delete', $admin));
    $this->assertFalse($entity->access('view', $admin));

    // No permissions cannot manage the entity.
    $no_permissions = $this->createUser([]);
    $this->assertFalse($entity->access('update', $no_permissions));
    $this->assertFalse($entity->access('delete', $no_permissions));
    $this->assertFalse($entity->access('view', $no_permissions));

    // Users with manage permissions, but not owners cannot manage the entity.
    $manage_permission = $this->createUser(['create and manage own entity notifications']);
    $this->assertFalse($entity->access('update', $manage_permission));
    $this->assertFalse($entity->access('delete', $manage_permission));
    $this->assertFalse($entity->access('view', $manage_permission));

    // Users with the permission and owner can manage the entity.
    $manage_permission_and_owner = $this->createUser(['create and manage own entity notifications']);
    $manage_permission_and_owner->entity_notify_preferences = [
      'target_id' => $entity->id(),
    ];
    $manage_permission_and_owner->save();
    $this->assertTrue($entity->access('update', $manage_permission_and_owner));
    $this->assertTrue($entity->access('delete', $manage_permission_and_owner));
    $this->assertFalse($entity->access('view', $manage_permission_and_owner));
  }

  /**
   * Test the create access.
   */
  public function testCreateAccess() {
    $admin = $this->createUser(['administer entity notifications']);
    $manage_permission = $this->createUser(['create and manage own entity notifications']);
    $forbidden_user = $this->createUser();

    $this->assertTrue($this->accessHandler->createAccess(NULL, $admin));
    $this->assertTrue($this->accessHandler->createAccess(NULL, $manage_permission));
    $this->assertFalse($this->accessHandler->createAccess(NULL, $forbidden_user));
  }

  /**
   * Test the field access.
   */
  public function testFieldAccess() {
    $forbidden_user = $this->createUser();
    $admin = $this->createUser(['administer entity notifications']);

    $this->assertTrue($admin->entity_notify_preferences->access('edit', $admin));
    $this->assertFalse($admin->entity_notify_preferences->access('edit', $forbidden_user));

    $manage_permission = $this->createUser(['create and manage own entity notifications']);
    $this->assertTrue($manage_permission->entity_notify_preferences->access('edit', $manage_permission));
    $this->assertFalse($manage_permission->entity_notify_preferences->access('edit', $forbidden_user));
  }

}
