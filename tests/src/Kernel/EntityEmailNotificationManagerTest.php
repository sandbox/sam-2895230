<?php

namespace Drupal\Tests\entity_notify\Kernel;

use Drupal\Core\Test\AssertMailTrait;
use Drupal\entity_notify\Entity\EntityNotifyPreference;
use Drupal\KernelTests\KernelTestBase;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\simpletest\UserCreationTrait;
use Drupal\workflows\Entity\Workflow;

/**
 * Test the entity notifications manager.
 *
 * @group entity_notify
 */
class EntityEmailNotificationManager extends KernelTestBase {

  use UserCreationTrait;
  use AssertMailTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'node',
    'system',
    'user',
    'entity_notify',
    'options',
    'content_moderation',
    'workflows',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->installSchema('system', ['sequences']);
    $this->installSchema('node', ['node_access']);
    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installEntitySchema('workflow');
    $this->installEntitySchema('content_moderation_state');
    $this->installEntitySchema('entity_notify_preference');
    $this->installConfig(['entity_notify', 'content_moderation']);

    \Drupal::configFactory()->getEditable('system.site')->set('name', 'test site')->save();
  }

  /**
   * Test the email manager.
   */
  public function testEmailManager() {
    $notification = $this->createNotification('node');

    $user = $this->createUser(['create and manage own entity notifications', 'access content']);
    $user->setEmail('user@example.com');
    $user->entity_notify_preferences[] = [
      'target_id' => $notification->id(),
    ];
    $user->save();

    $node = Node::create([
      'title' => 'Test Node',
      'type' => 'test',
    ]);
    $node->save();

    // An email is sent for new content.
    $this->assertCount(1, $this->getMails());
    $mail = $this->getMails()[0]['body'];
    $this->assertContains('A new content item has been created', $mail = $this->getMails()[0]['body']);

    // Updated content will contain a different message.
    $node->title = 'Updated node';
    $node->save();
    $this->assertContains('A content item has been updated', $mail = $this->getMails()[1]['body']);

    // Unpublished content will not send an email.
    $node->title = 'Updated node again';
    $node->setUnpublished();
    $node->save();
    $this->assertCount(2, $this->getMails());
  }

  /**
   * Test the emails with content moderation.
   */
  public function testModeratedEmails() {
    $notification = $this->createNotification('node', ['insert']);
    $user = $this->createUser(['create and manage own entity notifications', 'access content']);
    $user->setEmail('user@example.com');
    $user->entity_notify_preferences[] = [
      'target_id' => $notification->id(),
    ];
    $user->save();

    $node_type = NodeType::create([
      'type' => 'example',
    ]);
    $node_type->save();

    $workflow = Workflow::load('editorial');
    $workflow->getTypePlugin()->addEntityTypeAndBundle('node', 'example');
    $workflow->save();

    $node = Node::create([
      'title' => 'Test node',
      'type' => 'example',
      'moderation_state' => 'draft',
    ]);
    $node->save();
    $this->assertCount(0, $this->getMails());

    $node->moderation_state = 'published';
    $node->save();

    // The message for newly published content is 'created' not 'updated'.
    $this->assertContains('A new content item has been created', $mail = $this->getMails()[0]['body']);
  }

  /**
   * Create a notification entity.
   *
   * @param string $entity_type
   *   The entity type.
   * @param array $operations
   *   Operations to enable this for.
   *
   * @return \Drupal\entity_notify\Entity\EntityNotifyPreferenceInterface
   *   The entity.
   */
  protected function createNotification($entity_type, array $operations = ['update', 'insert']) {
    $notify_when_field = [];
    foreach ($operations as $operation) {
      $notify_when_field[] = [
        'value' => $operation,
      ];
    }
    $entity = EntityNotifyPreference::create([
      'entity_type' => $entity_type,
      'notify_when' => $notify_when_field,
    ]);
    $entity->save();
    return $entity;
  }

}
