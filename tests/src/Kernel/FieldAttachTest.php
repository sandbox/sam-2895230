<?php

namespace Drupal\Test\entity_notify\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\simpletest\UserCreationTrait;
use Drupal\user\Entity\User;

/**
 * Test attaching the field to the user.
 *
 * @group entity_notify
 */
class UserFieldAttachTest extends KernelTestBase {

  use UserCreationTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'system',
    'user',
    'entity_notify',
  ];

  /**
   * A test user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->installEntitySchema('user');
    $this->installSchema('system', 'sequences');
    $this->user = User::create([
      'name' => 'foo',
    ]);
  }

  /**
   * Test the field is attached to the user.
   */
  public function testUserAttachField() {
    $this->assertNotEmpty($this->user->getFieldDefinitions()['entity_notify_preferences']);
  }

}
