<?php

namespace Drupal\Tests\entity_notify\Functional;

use Drupal\entity_notify\Entity\EntityNotifyPreference;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\Entity\User;

/**
 * Test installation and uninstallation of the module.
 *
 * @group entity_notify
 * @todo Make this work.
 */
abstract class InstallationTest extends BrowserTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  public static $modules = [
    'entity_notify',
  ];

  /**
   * Test the installation and uninstallation.
   */
  public function testInstallUninstall() {
    $installed_modules = $this->container->get('module_handler')->getModuleList();
    $this->assertArrayHasKey('entity_notify', $installed_modules, 'The module was installed successfully.');

    // Create some field data.
    $user = User::load(1);
    $entity = EntityNotifyPreference::create([]);
    $entity->save();
    $user->entity_notify_preferences = [
      'target_id' => $entity->id(),
    ];
    $user->save();

    $this->container->get('module_installer')->uninstall(['entity_notify']);
    $this->rebuildContainer();

    $installed_modules = $this->container->get('module_handler')->getModuleList();
    $this->assertArrayNotHasKey('entity_notify', $installed_modules, 'The module was uninstalled successfully.');

    $pending_entity_updates = $this->container->get('entity.definition_update_manager')->getChangeSummary();
    $this->assertEmpty($pending_entity_updates);
  }

}
