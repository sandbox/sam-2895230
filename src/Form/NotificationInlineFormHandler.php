<?php

namespace Drupal\entity_notify\Form;

use Drupal\inline_entity_form\Form\EntityInlineForm;

/**
 * Inline entity form handler.
 */
class NotificationInlineFormHandler extends EntityInlineForm {

  /**
   * {@inheritdoc}
   */
  public function isTableDragEnabled($element) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getTableFields($bundles) {
    $fields = parent::getTableFields($bundles);
    $fields['label']['label'] = t('Notification type');
    return $fields;
  }

}
