<?php

namespace Drupal\entity_notify\Element;

use Drupal\Core\Render\Element\RenderElement;

/**
 * A digest email containing which entities need moderation.
 *
 * @RenderElement("entity_notify_immediate_email")
 */
class EntityNotifyImmediateEmail extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#theme' => 'entity_notify_immediate_email',
      '#pre_render' => [[$class, 'preRender']],
    ];
  }

  /**
   * Pre-render the element.
   */
  public static function preRender($element) {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    /** @var \Drupal\user\Entity\User $user */
    $entity = $element['#entity'];
    $user = $element['#user'];

    $definition = \Drupal::entityTypeManager()->getDefinition($entity->getEntityTypeId());

    $element['#username'] = $user->getDisplayName();
    $element['#is_new'] = $element['#operation'] === 'insert';
    $element['#website_name'] = \Drupal::config('system.site')->get('name');
    $element['#url'] = $entity->toUrl('canonical', ['absolute' => TRUE]);
    $element['#title'] = $entity->label();
    $element['#preferences_url'] = $user->toUrl('edit-form');
    $element['#type'] = $definition->getSingularLabel();

    $element['#cache']['max-age'] = 0;

    return $element;
  }

}
