<?php

namespace Drupal\entity_notify;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\entity_notify\Entity\EntityNotifyPreference;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Manage entity notifications.
 */
class EntityEmailNotificationManager implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The module configuration.
   *
   * @var \Drupal\entity_notify\EntityNotifyConfigurationInterface
   */
  protected $configuration;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The mail manager.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mail;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * EntityEmailNotificationManager constructor.
   */
  public function __construct(EntityNotifyConfigurationInterface $configuration, EntityTypeManagerInterface $entityTypeManager, MailManagerInterface $mail, RendererInterface $renderer) {
    $this->configuration = $configuration;
    $this->entityTypeManager = $entityTypeManager;
    $this->mail = $mail;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_notify.configuration'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.mail'),
      $container->get('renderer')
    );
  }

  /**
   * Send notifications for a given entity type.
   *
   * @param string $operation
   *   The operation the content is undergoing, 'update' or 'insert'.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   */
  public function sendNotifications($operation, EntityInterface $entity) {
    // Only send notifications for entity types we've enabled.
    if (!$this->configuration->isEntityTypeEnabled($entity->getEntityTypeId())) {
      return;
    }

    /** @var \Drupal\entity_notify\Entity\EntityNotifyPreference[] $notify_preferences */
    $notify_preferences = $this->entityTypeManager
      ->getStorage('entity_notify_preference')
      ->getQuery()
      ->condition('entity_type', $entity->getEntityTypeId(), '=')
      ->condition('notify_when', $operation, '=')
      ->execute();

    foreach ($notify_preferences as $notify_preference_id) {
      $notify_preference = EntityNotifyPreference::load($notify_preference_id);

      /** @var \Drupal\user\Entity\User $owner */
      if ($owner = $notify_preference->getOwner()) {

        // Users must be able to access the entity they are being notified
        // about.
        if (!$entity->access('view', $owner) || (($entity instanceof EntityPublishedInterface) && !$entity->isPublished())) {
          continue;
        }

        $this->mail->mail('entity_notify', 'entity_notify', $owner->getEmail(), $owner->getPreferredLangcode(), [
          'entity' => $entity,
          'user' => $owner,
          'operation' => $operation,
        ]);

      }
    }
  }

  /**
   * Implements hook_mail().
   */
  public function mail($key, &$message, $params) {
    if ($key !== 'entity_notify') {
      return;
    }
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $params['entity'];

    $email_body = [
      '#type' => 'entity_notify_immediate_email',
      '#entity' => $entity,
      '#user' => $params['user'],
      '#operation' => $params['operation'],
    ];

    $definition = $this->entityTypeManager->getDefinition($entity->getEntityTypeId());

    if ($params['operation'] === 'insert') {
      $message['subject'] = $this->t('New @type created', ['@type' => $definition->getSingularLabel()]);
    }
    else {
      $message['subject'] = $this->t('@type updated', ['@type' => ucfirst($definition->getSingularLabel())]);
    }

    $message['headers']['Content-Type'] = 'text/html; charset=UTF-8;';
    $message['body'][] = $this->renderer->renderPlain($email_body);
  }

}
