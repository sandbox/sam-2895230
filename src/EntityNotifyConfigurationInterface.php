<?php

namespace Drupal\entity_notify;

/**
 * Interface for the service that provides configuration options.
 */
interface EntityNotifyConfigurationInterface {

  /**
   * Get the enabled entity types as an options list.
   *
   * @return array
   *   An array of enabled entity types.
   */
  public function getEnabledEntityTypesOptionsList();

  /**
   * Check if a given entity type is enabled.
   */
  public function isEntityTypeEnabled($entity_type_id);

}
