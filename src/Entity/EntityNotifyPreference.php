<?php

namespace Drupal\entity_notify\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;

/**
 * An entity type for storing notification settings.
 *
 * @ContentEntityType(
 *   id = "entity_notify_preference",
 *   label = @Translation("Entity Notify Preference"),
 *   label_callback = "entity_notify_label_callback",
 *   handlers = {
 *     "access" = "Drupal\entity_notify\Entity\EntityNotifyPreferenceAccessControlHandler",
 *     "inline_form" = "Drupal\entity_notify\Form\NotificationInlineFormHandler",
 *     "form" = {
 *       "default" = "Drupal\entity_notify\Form\EntityNotifyPreferenceForm",
 *       "edit" = "Drupal\entity_notify\Form\EntityNotifyPreferenceForm",
 *     },
 *   },
 *   admin_permission = "administer entity notifications",
 *   base_table = "entity_notify_preference",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *   },
 * )
 */
class EntityNotifyPreference extends ContentEntityBase implements EntityNotifyPreferenceInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['entity_type'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Content Type'))
      ->setCardinality(1)
      ->setRequired(TRUE)
      ->setSetting('allowed_values_function', 'entity_notify_entity_type_callback')
      ->setDisplayOptions('form', [
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['notify_when'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Notify me when'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setRequired(TRUE)
      ->setSetting('allowed_values', [
        'insert' => t('Content is created'),
        'update' => t('Content is updated'),
      ])
      ->setDefaultValue(['insert'])
      ->setDisplayOptions('form', [
        'weight' => 2,
        'type' => 'options_buttons',
      ])
      ->setDisplayConfigurable('form', TRUE);

    return $fields;
  }

  /**
   * A label callback.
   *
   * @param \Drupal\entity_notify\Entity\EntityNotifyPreference $entity
   *   The entity to get the label for.
   *
   * @return string
   *   The entity label.
   */
  public static function labelCallback(EntityNotifyPreference $entity) {
    $definition = \Drupal::entityTypeManager()->getDefinition($entity->entity_type->value);
    return $definition->getLabel();
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    if ($uid = $this->getOwnerId()) {
      return User::load($uid);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    $owner = \Drupal::entityQuery('user')
      ->condition('entity_notify_preferences', $this->id(), '=')
      ->execute();
    if ($owner) {
      return array_shift($owner);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    throw new \Exception('Cannot change the owner of an entity notification entity.');
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    throw new \Exception('Cannot change the owner of an entity notification entity.');
  }

}
