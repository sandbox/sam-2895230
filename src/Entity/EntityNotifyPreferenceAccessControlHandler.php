<?php

namespace Drupal\entity_notify\Entity;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access control handler for the entity notify preference entity.
 */
class EntityNotifyPreferenceAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var EntityNotifyPreferenceInterface $entity */

    // These entities cannot be viewed outside an entity form.
    if ($operation === 'view') {
      return AccessResult::forbidden();
    }

    $result = AccessResult::forbidden();
    if ($account->hasPermission($this->entityType->getAdminPermission())) {
      $result = AccessResult::allowed();
    }
    elseif ($account->hasPermission('create and manage own entity notifications')) {
      // If the user owns the entity and we are updating or deleting, then we
      // grant access.
      if ($entity->getOwnerId() === $account->id() && in_array($operation, ['update', 'delete'])) {
        $result = AccessResult::allowed();
      }
    }

    $result->addCacheableDependency($account);
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    if ($account->hasPermission($this->entityType->getAdminPermission()) || $account->hasPermission('create and manage own entity notifications')) {
      return AccessResult::allowed();
    }
    else {
      return AccessResult::forbidden();
    }
  }

}
