<?php

namespace Drupal\entity_notify\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Interface for entity notification preferences.
 */
interface EntityNotifyPreferenceInterface extends ContentEntityInterface, EntityOwnerInterface {

}
