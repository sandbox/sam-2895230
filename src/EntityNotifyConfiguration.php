<?php

namespace Drupal\entity_notify;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Simple wrapper for entity notify configuration values.
 */
class EntityNotifyConfiguration implements EntityNotifyConfigurationInterface {

  /**
   * The module config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Create an instance of EntityNotifyConfiguration.
   */
  public function __construct(ConfigFactoryInterface $configFactory, EntityTypeManagerInterface $entityTypeManager) {
    $this->config = $configFactory->get('entity_notify.settings');
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function getEnabledEntityTypesOptionsList() {
    $entity_types = $this->config->get('entity_types');
    $options = [];
    foreach ($entity_types as $entity_type) {
      $definition = $this->entityTypeManager->getDefinition($entity_type);
      $options[$definition->id()] = $definition->getLabel();
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function isEntityTypeEnabled($entity_type_id) {
    return in_array($entity_type_id, $this->config->get('entity_types'));
  }

}
