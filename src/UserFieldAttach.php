<?php

namespace Drupal\entity_notify;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityAccessControlHandlerInterface;
use Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Manage attaching the new field to the user entity.
 */
class UserFieldAttach implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The entity definition update manager.
   *
   * @var \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface
   */
  protected $entityDefinitionUpdateManager;

  /**
   * The access control handler for the notify entity.
   *
   * @var \Drupal\Core\Entity\EntityAccessControlHandlerInterface
   */
  protected $notifyEntityAccessController;

  /**
   * Create an instance of UserFieldAttach.
   */
  public function __construct(EntityDefinitionUpdateManagerInterface $entityDefinitionUpdateManager, EntityAccessControlHandlerInterface $notifyEntityAccessController) {
    $this->entityDefinitionUpdateManager = $entityDefinitionUpdateManager;
    $this->notifyEntityAccessController = $notifyEntityAccessController;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.definition_update_manager'),
      $container->get('entity_type.manager')->getAccessControlHandler('entity_notify_preference')
    );
  }

  /**
   * Get the base field definition for the notify preferences field.
   *
   * @return \Drupal\Core\Field\BaseFieldDefinition
   *   A base field definition for the entity notify field.
   */
  protected function getBaseFieldDefinition() {
    return BaseFieldDefinition::create('entity_reference')
      ->setLabel($this->t('Notification Preferences'))
      ->setName('entity_notify_preferences')
      ->setTargetEntityTypeId('user')
      ->setDescription($this->t('Setup notifications for the types of content you wish to be notified about.'))
      ->setDisplayConfigurable('view', FALSE)
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayConfigurable('form', TRUE)
      ->setSetting('target_type', 'entity_notify_preference')
      ->setDisplayOptions('form', [
        'type' => 'inline_entity_form_complex',
        'weight' => -5,
        'settings' => [
          'override_labels' => TRUE,
          'label_singular' => 'Notification',
          'label_plural' => 'Notifications',
        ],
      ])
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ]);
  }

  /**
   * Implements hook_entity_field_access().
   */
  public function entityFieldAccess($operation, FieldDefinitionInterface $field_definition, AccountInterface $account, FieldItemListInterface $items = NULL) {
    if ($field_definition->getName() !== $this->getBaseFieldDefinition()->getName()) {
      return AccessResult::neutral();
    }
    // If a user does not have access to create the notification entity, deny
    // access to the field to prevent an empty field label from appearing.
    if ($operation === 'edit') {
      return $this->notifyEntityAccessController->createAccess(NULL, $account, [], TRUE);
    }
    return AccessResult::neutral();
  }

  /**
   * Implements hook_entity_base_field_info_alter().
   */
  public function entityBaseFieldInfoAlter(&$fields, EntityTypeInterface $entity_type) {
    if ($entity_type->id() !== 'user') {
      return;
    }
    $fields['entity_notify_preferences'] = $this->getBaseFieldDefinition();
  }

  /**
   * Install the field definition.
   */
  public function installFieldDefinition() {
    $this->entityDefinitionUpdateManager->installFieldStorageDefinition('entity_notify_preferences', 'user', 'user', $this->getBaseFieldDefinition());
  }

  /**
   * Uninstall the field definition.
   */
  public function uninstallFieldStorageDefinition() {
    foreach (User::loadMultiple() as $user) {
      $user->entity_notify_preferences = [];
      $user->save();
    }
    $field = $this->getBaseFieldDefinition();
    $this->entityDefinitionUpdateManager->uninstallFieldStorageDefinition($field);
  }

}
